import React, { useState, useCallback, useEffect, useMemo } from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate, Link } from 'react-router-dom';
import { AuthProvider, useAuth } from './AuthContext';
import Dashboard from './components/Dashboard';
import Backlog from './components/Backlog';
import Todo from './components/Todo';
import Ongoing from './components/Ongoing';
import Done from './components/Done';
import AddTaskForm from './components/AddTaskForm';
import MyTask from './components/MyTask';
import Login from './components/Login';
import './App.css';

const USERS = ['Admin', 'User1', 'User2', 'User3'];

function PrivateRoute({ children, adminOnly }) {
  const { user } = useAuth();
  
  if (!user) {
    return <Navigate to="/login" />;
  }

  if (adminOnly && user.role !== 'admin') {
    return <Navigate to="/dashboard" />;
  }

  return children;
}

function App() {
  const [tasks, setTasks] = useState([]);

  const addTask = useCallback((newTask) => {
    const taskToAdd = {
      ...newTask,
      id: Date.now().toString(),
      comments: [],
      attachments: [],
      assignedUsers: newTask.assignedUsers || [],
      progress: 0,
      submit: false,
      completionDate: null,
    };
    setTasks(prevTasks => [...prevTasks, taskToAdd]);
  }, []);

  const updateTask = useCallback((updatedTask) => {
    setTasks(prevTasks => prevTasks.map(task => {
      if (task.id === updatedTask.id) {
        if (updatedTask.status === 'done' && task.status !== 'done') {
          return { 
            ...updatedTask, 
            completionDate: new Date().toISOString().split('T')[0] // Format as YYYY-MM-DD
          };
        }
        return updatedTask;
      }
      return task;
    }));
  }, []);

  const deleteTask = useCallback((taskId) => {
    setTasks(prevTasks => prevTasks.filter(task => task.id !== taskId));
  }, []);

  useEffect(() => {
    const intervalId = setInterval(() => {
      const now = new Date();
      setTasks(prevTasks =>
        prevTasks.filter(task => {
          if (task.status !== 'done' || !task.completionDate) return true;
          const expiryDate = new Date(task.completionDate);
          expiryDate.setDate(expiryDate.getDate() + 2);
          return expiryDate > now;
        })
      );
    }, 60000);

    return () => clearInterval(intervalId);
  }, []);

  const memoizedTasks = useMemo(() => ({
    mytask: tasks.filter(t => t.status === 'mytask'),
    backlog: tasks.filter(t => t.status === 'backlog'),
    todo: tasks.filter(t => t.status === 'todo'),
    ongoing: tasks.filter(t => t.status === 'ongoing'),
    done: tasks.filter(t => t.status === 'done'),
  }), [tasks]);

  return (
    <AuthProvider>
      <Router>
        <div className="App">
          <header className="header">
            <h1><Link to="/" className="home-link">Task Management System</Link></h1>
            <AuthStatus />
          </header>
          
          <Navigation />
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/" element={<PrivateRoute><Dashboard tasks={tasks} /></PrivateRoute>} />
            <Route path="/backlog" element={<PrivateRoute><Backlog tasks={memoizedTasks.backlog} updateTask={updateTask} users={USERS} /></PrivateRoute>} />
            <Route path="/todo" element={<PrivateRoute><Todo tasks={memoizedTasks.todo} updateTask={updateTask} users={USERS} /></PrivateRoute>} />
            <Route path="/ongoing" element={<PrivateRoute><Ongoing tasks={memoizedTasks.ongoing} updateTask={updateTask} users={USERS} /></PrivateRoute>} />
            <Route path="/done" element={<PrivateRoute><Done tasks={memoizedTasks.done} updateTask={updateTask} deleteTask={deleteTask} users={USERS} /></PrivateRoute>} />
            <Route path="/add-task" element={<PrivateRoute adminOnly><AddTaskForm onAddTask={addTask} users={USERS} /></PrivateRoute>} />
            <Route path="/mytask" element={<PrivateRoute adminOnly><MyTask tasks={memoizedTasks.mytask} updateTask={updateTask} users={USERS} /></PrivateRoute>} />
          </Routes>
        </div>
      </Router>
    </AuthProvider>
  );
}

function AuthStatus() {
  const { user, logout } = useAuth();
  
  if (!user) {
    return <Link to="/login">Login</Link>;
  }

  return (
    <div className="header-buttons">
      {user.role === 'admin' && <Link to="/add-task" className="add-task-button">Add Task</Link>}
    </div>
  );
}

function Navigation() {
  const { user, logout } = useAuth();

  if (!user) return null;

  return (
    <nav className="top-menu">
      <div className="menu-items">
        <Link to="/">Dashboard</Link>
        {user.role === 'admin' && <Link to="/mytask">MyTask</Link>}
        <Link to="/backlog">Backlog</Link>
        <Link to="/todo">Todo</Link>
        <Link to="/ongoing">Ongoing</Link>
        <Link to="/done">Done</Link>
      </div>
      <button onClick={logout} className="logout-button">Logout</button>
    </nav>
  );
}

export default App;