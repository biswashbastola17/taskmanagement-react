

import React, { useState, useMemo, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';

const getCurrentDate = () => new Date().toISOString().split('T')[0];

const FormGroup = ({ id, label, children, error }) => (
  <div className="form-group">
    <label htmlFor={id}>{label}</label>
    {children}
    {error && <span className="error">{error}</span>}
  </div>
);

function AddTaskForm({ onAddTask, users, currentUser }) {
  const [formData, setFormData] = useState({
    project: '',
    title: '',
    description: '',
    startDate: getCurrentDate(),
    dueDate: '',
    priority: 'None',
    visibility: 'private',
    assignedUser: '',
    interestedUsers: [],
  });
  const [errors, setErrors] = useState({});
  const navigate = useNavigate();

  const handleInputChange = useCallback((e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  }, []);

  const handleVisibilityChange = useCallback((e) => {
    const visibility = e.target.value;
    setFormData((prevData) => ({
      ...prevData,
      visibility,
      assignedUser: visibility === 'public' ? '' : prevData.assignedUser,
      interestedUsers: visibility === 'public' ? prevData.interestedUsers : [],
    }));
  }, []);

  const validateForm = useCallback(() => {
    const newErrors = {};
    const requiredFields = ['project', 'title', 'startDate', 'dueDate', 'priority', 'visibility'];
    requiredFields.forEach(field => {
      if (!formData[field]) newErrors[field] = `${field.charAt(0).toUpperCase() + field.slice(1)} is required`;
    });
    if (formData.visibility === 'private' && !formData.assignedUser) {
      newErrors.assignedUser = 'Please assign a user for private tasks';
    }
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  }, [formData]);

  const handleSubmit = useCallback((e) => {
    e.preventDefault();
    const formIsValid = validateForm();
  
    if (formIsValid) {
      const newTask = {
        ...formData,
        status: determineInitialStatus(formData),
        comments: [],
        attachments: [],
        progress: 0
      };
      onAddTask(newTask);
  
      navigate(determineRedirectPath(formData));
    }
  }, [formData, validateForm, onAddTask, navigate]);

  const determineInitialStatus = (task) => {
    if (task.visibility === 'public') return 'backlog';
    if (task.visibility === 'private' && task.assignedUser === 'Admin') return 'mytask';
    return 'todo';
  };

  const determineRedirectPath = (task) => {
    if (task.visibility === 'public') return '/backlog';
    if (task.visibility === 'private' && task.assignedUser === 'Admin') return '/mytask';
    return '/todo';
  };

  const projectOptions = useMemo(() => [
    { value: '', label: 'Select Task' },
    { value: 'Member Complain', label: 'Member Complain' },
    { value: 'Account Related', label: 'Account Related' },
    { value: 'Board Related', label: 'Board Related' },
    { value: 'Marketing Department', label: 'Marketing Department' },
    { value: 'Other', label: 'Other' },
  ], []);

  const priorityOptions = useMemo(() => [
    { value: 'None', label: 'None' },
    { value: 'Low', label: 'Low' },
    { value: 'Medium', label: 'Medium' },
    { value: 'High', label: 'High' },
  ], []);

  return (
    <form onSubmit={handleSubmit} className="add-task-form">
      <FormGroup id="project" label="Project *" error={errors.project}>
        <select
          id="project"
          name="project"
          value={formData.project}
          onChange={handleInputChange}
          required
        >
          {projectOptions.map(option => (
            <option key={option.value} value={option.value}>{option.label}</option>
          ))}
        </select>
      </FormGroup>

      <FormGroup id="title" label="Task Name *" error={errors.title}>
        <input
          id="title"
          name="title"
          type="text"
          value={formData.title}
          onChange={handleInputChange}
          required
        />
      </FormGroup>

      <FormGroup id="description" label="Add Description">
        <textarea
          id="description"
          name="description"
          value={formData.description}
          onChange={handleInputChange}
        />
      </FormGroup>

      <div className="form-group date-inputs">
        <FormGroup id="startDate" label="Start Date *" error={errors.startDate}>
          <input
            id="startDate"
            name="startDate"
            type="date"
            value={formData.startDate}
            onChange={handleInputChange}
            required
          />
        </FormGroup>

        <FormGroup id="dueDate" label="Due Date *" error={errors.dueDate}>
          <input
            id="dueDate"
            name="dueDate"
            type="date"
            value={formData.dueDate}
            onChange={handleInputChange}
            required
          />
        </FormGroup>
      </div>

      <FormGroup id="priority" label="Priority *" error={errors.priority}>
        <select
          id="priority"
          name="priority"
          value={formData.priority}
          onChange={handleInputChange}
          required
        >
          {priorityOptions.map(option => (
            <option key={option.value} value={option.value}>{option.label}</option>
          ))}
        </select>
      </FormGroup>

      <FormGroup id="visibility" label="Visibility *" error={errors.visibility}>
        <select
          id="visibility"
          name="visibility"
          value={formData.visibility}
          onChange={handleVisibilityChange}
          required
        >
          <option value="private">Private</option>
          <option value="public">Public</option>
        </select>
      </FormGroup>

      {formData.visibility === 'private' && (
        <FormGroup id="assignedUser" label="Assign User *" error={errors.assignedUser}>
          <select
            id="assignedUser"
            name="assignedUser"
            value={formData.assignedUser}
            onChange={handleInputChange}
            required
          >
            <option value="">Select User</option>
            {users.map(user => (
              <option key={user} value={user}>{user}</option>
            ))}
          </select>
        </FormGroup>
      )}

      <div className="form-actions">
        <button type="submit" className="btn-primary">Add</button>
        <button type="button" className="btn-tertiary" onClick={() => navigate(-1)}>Cancel</button>
      </div>
    </form>
  );
}

export default AddTaskForm;