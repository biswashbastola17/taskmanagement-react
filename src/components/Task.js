import React, { useState, useCallback } from 'react';

function Task({ task, updateTask, deleteTask, users, currentUser }) {
  const [newComment, setNewComment] = useState('');
  const [progress, setProgress] = useState(task.progress || 0);
  const [isEditing, setIsEditing] = useState(false);

  const addComment = useCallback(() => {
    if (newComment.trim()) {
      updateTask({
        ...task,
        comments: [...task.comments, { text: newComment, user: currentUser, timestamp: new Date().toISOString() }]
      });
      setNewComment('');
    }
  }, [newComment, task, updateTask, currentUser]);

  const handleFileUpload = useCallback((e) => {
    const uploadedFile = e.target.files[0];
    if (uploadedFile) {
      const reader = new FileReader();
      reader.onloadend = () => {
        updateTask({
          ...task,
          attachments: [...task.attachments, { name: uploadedFile.name, url: reader.result }]
        });
      };
      reader.readAsDataURL(uploadedFile);
    }
  }, [task, updateTask]);

  const handleProgressChange = useCallback((e) => {
    const newProgress = parseInt(e.target.value, 10);
    setProgress(newProgress);
    updateTask({ ...task, progress: newProgress });
  }, [task, updateTask]);

  const handleSubmit = useCallback(() => {
    updateTask({ ...task, submit: !task.submit });
    alert('Task saved successfully!');
  }, [task, updateTask]);

  const handleDelete = useCallback(() => {
    deleteTask(task.id);
  }, [deleteTask, task.id]);

   const toggleEditMode = useCallback(() => {
    setIsEditing(prev => !prev);
  }, []);

  return (
    <div className="task">
      <div className="task-header">
        <h3>{task.title}</h3>
        <div className="task-actions">
          <button onClick={toggleEditMode}>
            {isEditing ? 'Cancel' : 'Edit'}
          </button>
          <button onClick={handleDelete}>Delete</button>
        </div>
      </div>

      {isEditing ? (
        <div className="task-edit">
         <label>
            Task Name:
            <input name="title" value={editedTask.title} onChange={handleChange} />
          </label>
          <label>
            Project:
            <input name="project" value={editedTask.project} onChange={handleChange} />
          </label>
          <label>
            Start Date:
            <input name="startDate" type="date" value={editedTask.startDate} onChange={handleChange} />
          </label>
          <label>
            Due Date:
            <input name="dueDate" type="date" value={editedTask.dueDate} onChange={handleChange} />
          </label>
          <label>
            Assigned To:
            <select
              id="assignedUser"
              name="assignedUser"
              value={formData.assignedUser}
              onChange={handleInputChange}
              required
            >
              <option value="">Select User</option>
              {users.map((user) => (
                <option key={user} value={user}>
                  {user}
                </option>
              ))}
            </select>
          </label>
          <label>
            Priority:
            <input name="priority" value={editedTask.priority} onChange={handleChange} />
          </label>
          <label>
            % Complete:
            <input
              name="progress"
              type="number"
              min="0"
              max="100"
              value={editedTask.progress}
              onChange={handleChange}
            />
          </label>
          <label>
            Status:
            <select name="status" value={editedTask.status} onChange={handleChange}>
              <option value="Backlog">Backlog</option>
              <option value="Todo">Todo</option>
              <option value="Ongoing">Ongoing</option>
              <option value="MyTask">My Task</option>
              <option value="Done">Done</option>
            </select>
          </label>
          <button onClick={toggleEditMode}>Save</button>
        </div>
      ) : (
        <div className="task-details">
          <p><strong>Description:</strong> {task.description}</p>
          <p><strong>Project:</strong> {task.project}</p>
          <p><strong>Start Date:</strong> {task.startDate}</p>
          <p><strong>Due Date:</strong> {task.dueDate}</p>
          <p><strong>Priority:</strong> {task.priority}</p>
          <p><strong>Status:</strong> {task.status}</p>
          <p><strong>Assigned to:</strong> {task.assignedUser || 'Unassigned'}</p>
          <div className="task-progress">
            <label><strong>Progress:</strong></label>
            <input
              type="range"
              min="0"
              max="100"
              value={progress}
              onChange={handleProgressChange}
            />
            <span>{progress}%</span>
          </div>
        </div>
      )}

<div className="task-interaction">
        <div className="comment-section">
          <h4>Comments</h4>
          {task.comments.map((comment, index) => (
            <div key={index} className="comment">
              <p>{comment.text}</p>
              <small>{comment.user} - {new Date(comment.timestamp).toLocaleString()}</small>
            </div>
          ))}
          <textarea
            value={newComment}
            onChange={(e) => setNewComment(e.target.value)}
            placeholder="Add a comment"
          />
          <button onClick={addComment}>Add Comment</button>
        </div>
        <div className="file-upload-section">
          <h4>Attachments</h4>
          {task.attachments.map((attachment, index) => (
            <div key={index} className="attachment">
              <a href={attachment.url} target="_blank" rel="noopener noreferrer">{attachment.name}</a>
            </div>
          ))}
          <input type="file" onChange={handleFileUpload} />
        </div>
      </div>

      <button onClick={handleSubmit} className="submit-button">
        {task.submit ? 'Unsubmit' : 'Submit'} Task
      </button>
    </div>
  );
}

export default React.memo(Task);