import React from 'react';
import { Link } from 'react-router-dom';
import { useAuth } from '../AuthContext';
import './Dashboard.css';

function Dashboard({ tasks }) {
  const { user } = useAuth();

  const filteredTasks = user.role === 'admin' 
    ? tasks 
    : tasks.filter(t => t.status !== 'mytask');

  const taskCounts = {
    backlog: filteredTasks.filter(t => t.status === 'backlog').length,
    todo: filteredTasks.filter(t => t.status === 'todo').length,
    ongoing: filteredTasks.filter(t => t.status === 'ongoing').length,
    done: filteredTasks.filter(t => t.status === 'done').length,
  };

  if (user.role === 'admin') {
    taskCounts.mytask = tasks.filter(t => t.status === 'mytask').length;
  }

  return (
    <div className="dashboard-page">
      <h2>Dashboard</h2>
      <div className="task-summary">
        {Object.entries(taskCounts).map(([status, count]) => (
          <div key={status} className="summary-item">
            <h3>{status.charAt(0).toUpperCase() + status.slice(1)}</h3>
            <p>{count}</p>
            <Link to={`/${status}`}>View {status}</Link>
          </div>
        ))}
      </div>
      <div className="total-tasks">
        <h3>Total Tasks: {filteredTasks.length}</h3>
      </div>
    </div>
  );
}

export default Dashboard;
