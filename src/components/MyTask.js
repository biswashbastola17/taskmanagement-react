


import React from 'react';
import TaskDetails from './TaskDetails';

function MyTask({ tasks, updateTask, deleteTask, users, currentUser }) {
  const groupedTasks = tasks.reduce((acc, task) => {
    if (!acc[task.project]) {
      acc[task.project] = [];
    }
    acc[task.project].push(task);
    return acc;
  }, {});

  return (
    <div className="my-task-page">
      <h2>My Tasks</h2>
      {Object.keys(groupedTasks).length === 0 ? (
        <p>No tasks assigned to you.</p>
      ) : (
        Object.keys(groupedTasks).map(project => (
          <div key={project}>
            <h3>{project}</h3>
            <table className="task-table">
              <thead>
                <tr>
                  <th><input type="checkbox" /></th>
                  <th>ID</th>
                  <th>Task Name</th>
                  <th>Project</th>
                  <th>Start Date</th>
                  <th>Due Date</th>
                  <th>Assigned To</th>
                  <th>Priority</th>
                  <th>% Complete</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {groupedTasks[project].map(task => (
                  <React.Fragment key={task.id}>
                    <tr>
                      <td><input type="checkbox" /></td>
                      <td>{task.id}</td>
                      <td>{task.title}</td>
                      <td>{task.project}</td>
                      <td>{task.startDate}</td>
                      <td>{task.dueDate}</td>
                      <td>{task.assignedUser}</td>
                      <td><span className={`priority ${task.priority.toLowerCase()}`}>{task.priority}</span></td>
                      <td>
                        <div className="progress-bar">
                          <div className="progress" style={{ width: `${task.progress || 0}%` }}></div>
                        </div>
                        <span className="percent-complete">{task.progress || 0}%</span>
                      </td>
                      <td>
                        <button onClick={() => updateTask({ ...task, showDetails: !task.showDetails })}>
                          {task.showDetails ? 'Hide' : 'Show'} Details
                        </button>
                      </td>
                    </tr>
                    {task.showDetails && (
                      <tr>
                        <td colSpan="10">
                          <TaskDetails
                            task={task}
                            updateTask={updateTask}
                            deleteTask={deleteTask}
                            users={users}
                            currentUser={currentUser}
                          />
                        </td>
                      </tr>
                    )}
                  </React.Fragment>
                ))}
              </tbody>
            </table>
          </div>
        ))
      )}
    </div>
  );
}

export default MyTask;
