import React, { useState, useCallback } from 'react';

function TaskDetails({ task, updateTask, deleteTask, users, currentUser, projectOptions = [] }) {
  const [editMode, setEditMode] = useState(false);
  const [editedTask, setEditedTask] = useState({ ...task });
  const [comment, setComment] = useState('');
  const [file, setFile] = useState(null);

  const handleEdit = () => {
    setEditedTask({ ...task });
    setEditMode(true);
  };

  const handleSave = () => {
    updateTask(editedTask);
    setEditMode(false);
  };

  const handleDelete = () => {
    deleteTask(task.id);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setEditedTask({ ...editedTask, [name]: value });
  };

  const handleCommentChange = (e) => {
    setComment(e.target.value);
  };

  const handleFileChange = (e) => {
    setFile(e.target.files[0]);
  };

  const handleCommentSubmit = useCallback(() => {
    if (comment.trim()) {
      const newComment = {
        text: comment,
        user: currentUser,
        timestamp: new Date().toISOString(),
      };
      updateTask({
        ...task,
        comments: [...(task.comments || []), newComment],
      });
      setComment('');
    }
  }, [comment, currentUser, task, updateTask]);

  const handleFileUpload = useCallback(() => {
    if (file) {
      updateTask({
        ...task,
        attachments: [...(task.attachments || []), { name: file.name, url: URL.createObjectURL(file) }],
      });
      setFile(null);
    }
  }, [file, task, updateTask]);

  return (
    <div className="task-details">
      {editMode ? (
        <div>
          <h2>Edit Task</h2>
          <table className="edit-table">
            <tbody>
              {/* <tr>
                <th>Title:</th>
                <td><input id="title" name="title" value={editedTask.title} onChange={handleChange} /></td>
              </tr>
              <tr>
                <th>Project:</th>
                <td><input id="project" name="project" value={editedTask.project} onChange={handleChange} /></td>
              </tr>
              <tr>
                <th>Start Date:</th>
                <td><input id="startDate" name="startDate" type="date" value={editedTask.startDate} onChange={handleChange} /></td>
              </tr> */}
              <tr>
                <th>Due Date:</th>
                <td><input id="dueDate" name="dueDate" type="date" value={editedTask.dueDate} onChange={handleChange} /></td>
              </tr>
              <tr>
                <th>Assigned To:</th>
                <td>
                  <select id="assignedUser" name="assignedUser" value={editedTask.assignedUser} onChange={handleChange}>
                    {users.map(user => (
                      <option key={user} value={user}>{user}</option>
                    ))}
                  </select>
                </td>
              </tr>
              <tr>
                <th>Priority:</th>
                <td><input id="priority" name="priority" value={editedTask.priority} onChange={handleChange} /></td>
              </tr>
              <tr>
                <th>Progress:</th>
                <td><input id="progress" name="progress" type="number" value={editedTask.progress} onChange={handleChange} /></td>
              </tr>
              <tr>
                <th>Status:</th>
                <td>
                  <select id="status" name="status" value={editedTask.status} onChange={handleChange}>
                    <option value="backlog">Backlog</option>
                    <option value="todo">Todo</option>
                    <option value="ongoing">Ongoing</option>
                    <option value="done">Done</option>
                  </select>
                </td>
              </tr>
            </tbody>
          </table>
          <div className="button-group">
            <button onClick={handleSave}>Save</button>
            <button onClick={() => setEditMode(false)}>Cancel</button>
          </div>
        </div>
      ) : (
        <div>
          <h2>Task Details</h2>
          <table className="detail-table">
            <tbody>
              <tr>
                <th>Title:</th>
                <td>{task.title}</td>
                <th>Project:</th>
                <td>{task.project}</td>
              </tr>
              <tr>
                <th>Start Date:</th>
                <td>{task.startDate}</td>
                <th>Due Date:</th>
                <td>{task.dueDate}</td>
              </tr>
              <tr>
                <th>Assigned To:</th>
                <td>{task.assignedUser}</td>
                <th>Priority:</th>
                <td>{task.priority}</td>
              </tr>
              <tr>
                <th>Progress:</th>
                <td>{task.progress}%</td>
                <th>Status:</th>
                <td>{task.status}</td>
              </tr>
            </tbody>
          </table>
          <div className="button-group">
            <button onClick={handleEdit}>Edit</button>
          </div>

          <div className="comments-section">
            <h4>Comments</h4>
            {task.comments && task.comments.map((comment, index) => (
              <div key={index} className="comment">
                <p>{comment.text}</p>
                <small>{comment.user} - {new Date(comment.timestamp).toLocaleString()}</small>
              </div>
            ))}
            <textarea
              value={comment}
              onChange={handleCommentChange}
              placeholder="Add a comment..."
            />
            <button onClick={handleCommentSubmit}>Submit Comment</button>
          </div>
          <div className="file-upload-section">
            <h4>Attachments</h4>
            {task.attachments && task.attachments.map((attachment, index) => (
              <div key={index} className="attachment">
                <a href={attachment.url} target="_blank" rel="noopener noreferrer">{attachment.name}</a>
              </div>
            ))}
            <input type="file" onChange={handleFileChange} />
            <button onClick={handleFileUpload}>Upload File</button>
          </div>
        </div>
      )}
    </div>
  );
}

export default TaskDetails;