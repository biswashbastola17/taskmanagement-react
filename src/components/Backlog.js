import React from 'react';
import { Link } from 'react-router-dom';
import TaskDetails from './TaskDetails';
import { useAuth } from '../AuthContext';

function Backlog({ tasks, updateTask, deleteTask, users }) {
  const { user } = useAuth();
  const groupedTasks = tasks.reduce((acc, task) => {
    if (!acc[task.project]) {
      acc[task.project] = [];
    }
    acc[task.project].push(task);
    return acc;
  }, {});

  return (
    <div className="backlog-page">
      <h2>Backlog</h2>
      {/* {user.role === 'admin' && <Link to="/add-task" className="add-task-button">Add Task</Link>} */}
      {Object.keys(groupedTasks).length === 0 ? (
        <p>No tasks in backlog.</p>
      ) : (
        Object.keys(groupedTasks).map(project => (
          <div key={project}>
            <h3>{project}</h3>
            <table className="task-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Task Name</th>
                  <th>Start Date</th>
                  <th>Due Date</th>
                  <th>Assigned To</th>
                  <th>Priority</th>
                  <th>% Complete</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {groupedTasks[project].map(task => (
                  <React.Fragment key={task.id}>
                    <tr>
                      <td>{task.id}</td>
                      <td>{task.title}</td>
                      <td>{task.startDate}</td>
                      <td>{task.dueDate}</td>
                      <td>{task.assignedUser}</td>
                      <td><span className={`priority ${task.priority.toLowerCase()}`}>{task.priority}</span></td>
                      <td>
                        <div className="progress-bar">
                          <div className="progress" style={{ width: `${task.progress || 0}%` }}></div>
                        </div>
                        <span className="percent-complete">{task.progress || 0}%</span>
                      </td>
                      <td>
                        <button onClick={() => updateTask({ ...task, showDetails: !task.showDetails })}>
                          {task.showDetails ? 'Hide' : 'Show'} Details
                        </button>
                      </td>
                    </tr>
                    {task.showDetails && (
                      <tr>
                        <td colSpan="8">
                          <TaskDetails
                            task={task}
                            updateTask={updateTask}
                            deleteTask={deleteTask}
                            users={users}
                          />
                        </td>
                      </tr>
                    )}
                  </React.Fragment>
                ))}
              </tbody>
            </table>
          </div>
        ))
      )}
    </div>
  );
}

export default Backlog;